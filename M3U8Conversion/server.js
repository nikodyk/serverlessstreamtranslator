'use strict';

console.log('Loading function');

const aws = require('aws-sdk');
const https = require('https');
const async = require('async');
const ffmpeg = require('ffmpeg');
const fs = require('fs');
const id3Parser = require('id3-parser');

const s3 = new aws.S3();
var sqs = new aws.SQS({ region: process.env.region });

const RAW_BUCKET = process.env.rawBucket;
const QUEUE_URL = process.env.queueUrl;
const PROCESS_MESSAGE = 'process-message';
const MAX_FRAMES = process.env.maxFrames

function HandleContentRequest(url, language, callback) {
    var httpType = url.indexOf('https://') == 0 ? 'https://' : 'http://';
    var host = url.substr(httpType.length, url.indexOf('/', httpType.length + 1) - httpType.length);
    console.info(`Host: ${host}`);
    var path = url.substr(url.indexOf('/', httpType.length + 1));
    console.info(`Path: ${path}`);

    var header = null;
    var currentContent = '';
    var counter = 1;
    
    var request = https.get({
        hostname: host,
        port: 443,
        method: 'GET',
        encoding: null,
        path: path,
        agent: false,
        keepAlive: true,
        Accept:'audio/mpeg3;audio/x-mpeg-3;video/mpeg;video/x-mpeg;text/xml',
        headers: {
            Connection: "Keep-Alive",
            "Accept-Encoding":"identity;q=1, *;q=0"
        }
    }, (res) => {
        console.info('handling http request');
        console.log('statusCode:', res.statusCode);
        console.log('headers:', res.headers);
        var header = null;
        var headerLength = 0;
        var currentContent = '';
        var counter = 1;
        res.on('data', data => {
            console.info(`handling data ${data.length}`);
            currentContent += data;
            
            if ((currentContent.length - headerLength) > 10 * 1028 * 1028) {
                console.info(`MP3 Segment recieved, and saving ${counter}`);
                s3.putObject({
                    Bucket: RAW_BUCKET,
                    Key: `${counter}.ts`,
                    Body: currentContent
                }, function () {

                });
                counter++;

                if (currentContent.length > 10 && header == null) {

                    id3Parser.parse(currentContent).then(tag => {
                        console.log(tag);
                    });
                    var coreHeader = currentContent.substr(0, 10);
                    var extendedLengthStr = currentContent.substr(11, 4);
                    var number = extendedLengthStr.charCodeAt(0) << 3;
                    var number = number | (extendedLengthStr.charCodeAt(1) << 2);
                    var number = number | (extendedLengthStr.charCodeAt(2) << 1);
                    var number = number | extendedLengthStr.charCodeAt(1);

                    header = currentContent.substr(0, number + 10);
                    headerLength = header.length;
                }

                currentContent = header;
            }
        });

        res.on('end', () => {
            console.info('Response ended');
        });
    }).on('error', (err) => {
        console.error(err);
    });
}

function invokePoller(functionName, message) {
    const payload = {
        operation: PROCESS_MESSAGE,
        message,
    };
    const params = {
        FunctionName: functionName,
        InvocationType: 'Event',
        Payload: new Buffer(JSON.stringify(payload)),
    };
    return new Promise((resolve, reject) => {
        Lambda.invoke(params, (err) => (err ? reject(err) : resolve()));
    });
}

function processMessage(message, callback) {
    console.log(message);

    HandleContentRequest(message.url, message.language, function () {
    });
    
    // delete message
    const params = {
        QueueUrl: QUEUE_URL,
        ReceiptHandle: message.ReceiptHandle,
    };
    sqs.deleteMessage(params, (err) => callback(err, message));
}

function poll(functionName, callback) {
    const params = {
        QueueUrl: QUEUE_URL,
        MaxNumberOfMessages: 1,
        VisibilityTimeout: 10,
    };
    // batch request messages
    sqs.receiveMessage(params, (err, data) => {
        if (err) {
            return callback(err);
        }
        console.info(`Checking data url ${JSON.stringify(data)}`);
        if (data.url != undefined) {
            console.info(`Processing data url`);
            invokePoller(functionName, data);
        } else {

            console.info(`Processing data message`);
            // for each message, reinvoke the function
            const promises = data.Messages.map((message) => invokePoller(functionName, message));
            // complete when all invocations have been made
            Promise.all(promises).then(() => {
                const result = `Messages received: ${data.Messages.length}`;
                console.log(result);
                callback(null, result);
            });
        }
    });
}

exports.handler = (event, context, callback) => {
    try {
        if (event.operation === PROCESS_MESSAGE) {
            // invoked by poller
            console.info('Calling process message with non-function');
            processMessage(event.message, callback);
        } else {
            // invoked by schedule
            //poll(context.functionName, callback);
            console.info(`Content: ${JSON.stringify(event)}`);
            HandleContentRequest(event.url, event.language, callback);
        }
    } catch (err) {
        callback(err);
    }
};
